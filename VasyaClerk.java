//Vasya - Clerk
//6 kyu

/*
Description: 

The new "Avengers" movie has just been released! There are a lot of people at the cinema box office standing in a huge line. Each of them has a single 100, 50 or 25 dollars bill. A "Avengers" ticket costs 25 dollars.

Vasya is currently working as a clerk. He wants to sell a ticket to every single person in this line.

Can Vasya sell a ticket to each person and give the change if he initially has no money and sells the tickets strictly in the order people follow in the line?

Return YES, if Vasya can sell a ticket to each person and give the change. Otherwise return NO.
Examples:

// === JavaScript ==

tickets([25, 25, 50]) // => YES 
tickets([25, 100])    
        // => NO. Vasya will not have enough money to give change to 100 dollars


*/


//Return YES, if Vasya can sell a ticket to each person and give the change. Otherwise return NO.
public class Line {
  public static String Tickets(int[] peopleInLine)
  {
  int[] licznik = {0, 0, 0};
  for (int i = 0; i<peopleInLine.length; i++){
   if (peopleInLine[i] ==25) licznik[0]+=1;
   if (peopleInLine[i] ==50) {
     if (licznik[0]<1) return "NO";
     else {
     licznik[1]+=1;
     licznik[0]-=1;
     }
   }
   if (peopleInLine[i] ==100) {
     if ((licznik[0]>0) && (licznik[1]>0)){
       licznik[0]-=1;
       licznik[1]-=1;
       licznik[2]+=1;
     }
     else if (licznik[0]>2){
       licznik[0]-=3;
       licznik[2]+=1;
     }
     else return "NO";
   }
   }
   return "YES";
  }
}