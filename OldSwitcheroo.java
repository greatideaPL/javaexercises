//The old switcheroo
//7 kyu

/*
Description: 

Write a function

vowel2index(str)

that takes in a string and replaces all the vowels [a,e,i,o,u] with their respective positions within that string.
E.g:

vowel2index('this is my string') == 'th3s 6s my str15ng'
vowel2index('Codewars is the best site in the world') == 'C2d4w6rs 10s th15 b18st s23t25 27n th32 w35rld'
vowel2index('') == ''

*/


import java.util.*;
public class Kata {
  public static String vowel2Index(String s) {

      String newS = ""; 
      for (int i=0; i<s.length(); i++){
        char temp = s.charAt(i);
        if ((temp == 'a') || (temp == 'e') || (temp == 'i') || (temp == 'o')
        || (temp == 'u')) {
          int number=i+1;
          newS+=Integer.toString(number);
        }
        else newS+=String.valueOf(temp);
      }
      return newS;
  }     
 
}